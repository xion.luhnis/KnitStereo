/* jshint esversion: 6 */
"use strict";

/* global process, console, require, setTimeout */

function usage(){
  console.log('Usage: ./scan.js [options] device led1 led2 ...');
  console.log('Options:');
  console.log(' -n      do not take pictures, just trigger leds');
  console.log(' -o $d   output in directory $d');
  console.log(' -q      quiet mode');
  console.log(' -i $n   time to wait (3000)');
  console.log(' -t $n   startup time (2000)');
  console.log('');
}

if(process.argv.length < 4){
  usage();
  process.exit(1);
} else if(process.argv[2] == '-h'){
  usage();
  process.exit(0);
}

// modules
const SerialPort = require('serialport');
const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;

let quiet = false;
let noPictures = false;
let outputDir = '.';
let argp = 2;
let options = true;
let timeinit = 3000;
let timeout = 2000;
while(argp < process.argv.length){
  switch(process.argv[argp]){
    case '-n': noPictures = true; break;
    case '-o': 
      outputDir = process.argv[argp+1]; ++argp;
      if(!fs.existsSync(outputDir))
        fs.mkdirSync(outputDir);
      break;
    case '-q': quiet = true; break;
    case '-t': timeout = parseInt(process.argv[argp+1]); ++argp; break;
    case '-i': timeout = parseInt(process.argv[argp+1]); ++argp; break;
    default: options = false; break;
  }
  if(!options)
    break;
  argp = argp + 1;
}

let port = new SerialPort(process.argv[argp], {
  baudRate: 115200,
  dataBits: 8,
  parity: 'none',
  flowControl: false,
  autoOpen: false
});
port.on('error', function(err) {
    console.log('> Port error: ', err.message);
});
port.on('data', function (data) {
  if(!quiet)
    process.stdout.write(data.toString().replace("\n", ''));
});

port.open(function(err) {
  if(err){
    console.log("> Error: ", err.message);
  } else {
    console.log('> Starting');
    setTimeout(scan, 3000);
  }
});

function send(str){
  console.log('> Sending: ' + str);
  return new Promise((resolve, reject) => {
    port.write(str, function(err){
      if(err){
        console.log('> Send error: ', err.message);
        reject();
      } else {
        setTimeout(resolve, timeout);
      }
    });
  });
}

function leftPad(str, width, fill){
  if(!fill) fill = '0';
  while(str.length < fill)
    str = fill + str;
  return str;
}

function trigger(num, led){
  return new Promise((resolve, reject) => {
    console.log('> Triggering camera');
    let fname = num + '_' + led + '.jpg';
    let script = 'gphoto2 --capture-image-and-download --filename=' + fname;
    if(outputDir !== '.'){
      script += ' && mv ' + fname + ' ' + outputDir;
    }
    exec(script, (error, stdout, stderr) => {
      if (error) {
        console.log('> Exec error: ', error.message);
        console.log('> STDErr: ', stderr);
        reject();
      } else {
        console.log('> STDOut: ', stdout);
        resolve();
      }
    });
  });
}

async function scan(){
  for(let i = argp + 1, j = 0; i < process.argv.length; ++i, ++j){
    await send('p ' + process.argv[i]);

    if(noPictures)
      continue;
    // trigger camera
    await trigger(j, process.argv[i]);
  }
  send('c');
  setTimeout(()=> {
    console.log('> Done');
    port.close();
  }, timeout);
}
