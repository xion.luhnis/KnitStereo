#!/usr/bin/env bash

# find pid of process for PTP camera from MacOS
ptp_id=$(ps x | grep PTPCamera | grep /System | cut -d' ' -f1)

# see blog.dcclark.net/2009/05/how-to-gphoto-primer.html
if [[ -n "$ptp_id" ]]; then
  echo "Killing PTP process #$ptp_id"
  kill -9 "$ptp_id"
  # killall PTPCamera
fi

