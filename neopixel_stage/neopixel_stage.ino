#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

#define NUM_LEDS 120

#define BRIGHTNESS 255

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);

byte neopix_gamma[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };


void setup() {
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  // End of trinket special code
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  Serial.begin(115200);
}

uint16_t from = 36, to = 103;
uint16_t skip_from = 62, skip_to = 80;

int red = 0, green = 0, blue = 0, white = 255;

void loop() {

  Serial.println("Waiting for command\n");
  usage();
  
  // wait for command
  while(!Serial.available()){
    delay(100);
  }
  char command = Serial.read();
  int num = 0;
  switch(command){
    
    case 'c':
      clearPixels();
      break;
      
    case 'p': 
      num = readInt();
      singlePixel(num);
      break;
      
    case 'P':
      num = readInt(); // number of pixels to switch on
      // clear all
      for(uint16_t i=0; i<strip.numPixels(); i++)
        strip.setPixelColor(i, strip.Color(0,0,0,0));
      for(uint16_t i = 0; i < num; ++i){
        num = readInt();
        strip.setPixelColor(num, strip.Color(red, green, blue, white));
      }
      strip.show();
      break;

    case 'r':
    case 'R':
    case 'g':
    case 'G':
    case 'b':
    case 'B':
    case 'w':
    case 'W':
      num = readInt();
      if(num >= 0 && num <= 255){
        if(command == 'r' || command == 'R') red = num;
        else if(command == 'g' || command == 'G') green = num;
        else if(command == 'b' || command == 'B') blue = num;
        else if(command == 'w' || command == 'W') white = num;
        Serial.print("R="); Serial.println(red);
        Serial.print("G="); Serial.println(green);
        Serial.print("B="); Serial.println(blue);
        Serial.print("W="); Serial.println(white);
      } else {
        Serial.print("Invalid number: ");
        Serial.println(num);
        Serial.println("Should be within [0;255]");
      }
      break;

    case 'l':
    case 'L':
      for(uint16_t i = 0; i < strip.numPixels(); ++i){
        singlePixel(i);
        delay(200);
      }
      clearPixels();
      break;

    case 'a':
    case 'A':
      allPixels();
      break;

    case 'h':
    case 'H':
      usage();
      break;
      
    default:
      return;
  }
}

int readInt() {
  String number;
  do {
    char c = Serial.read();
    if(c >= '0' && c <= '9')
      number += c;
    else if(c == ' ' && number.length() == 0) // only accept before number
      continue;
    else
      break;
  } while(true);
  return number.toInt();
}

void singlePixel(uint16_t pixelID) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, i == pixelID ? strip.Color(red, green, blue, white) : strip.Color(0,0,0,0));
  }
  strip.show();
}

void allPixels(){
  for(uint16_t i=0; i<strip.numPixels(); i++)
    strip.setPixelColor(i, strip.Color(red, green, blue, white));
  strip.show();
}

void clearPixels(){
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0,0,0,0));
  }
  strip.show();
}

void usage(){
  Serial.println("\tc     = clear\n");
  Serial.println("\tp num = pixel on\n");
  Serial.println("\tP N n_1 n_2 ... n_N = multiple pixels on\n");
  Serial.println("\t$x N  = set component $x to N, N in [0;255], $x in {r,g,b,w}\n");
  Serial.println("\ta     = set all pixels on\n");
  Serial.println("");
}

