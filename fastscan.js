/* jshint esversion: 6 */
"use strict";

/* global process, console, require, setTimeout */

function usage(){
  console.log('Usage: ./fastscan.js [options] device led1 led2 ...');
  console.log('Options:');
  console.log(' -n      do not take pictures, just trigger leds');
  console.log(' -o $d   output in directory $d');
  console.log(' -q      quiet mode');
  console.log(' -i $n   time to wait (3000)');
  console.log(' -t $n   startup time (2000)');
  console.log('');
}

if(process.argv.length < 4){
  usage();
  process.exit(1);
} else if(process.argv[2] == '-h'){
  usage();
  process.exit(0);
}

// modules
const SerialPort = require('serialport');
const fs = require('fs');
const path = require('path');
const { spawn } = require('child_process');

let quiet = false;
let noPictures = false;
let outputDir = '.';
let argp = 2;
let options = true;
let timeinit = 3000;
let timeout = 2000;
while(argp < process.argv.length){
  switch(process.argv[argp]){
    case '-n': noPictures = true; break;
    case '-o': 
      outputDir = process.argv[argp+1]; ++argp;
      if(!fs.existsSync(outputDir))
        fs.mkdirSync(outputDir);
      break;
    case '-q': quiet = true; break;
    case '-t': timeout = parseInt(process.argv[argp+1]); ++argp; break;
    case '-i': timeout = parseInt(process.argv[argp+1]); ++argp; break;
    default: options = false; break;
  }
  if(!options)
    break;
  argp = argp + 1;
}

let gphoto2 = spawn('gphoto2', ['--shell', '--filename=capture.jpg']);
if(fs.existsSync('capture.jpg')){
  fs.unlinkSync('capture.jpg');
}

let port = new SerialPort(process.argv[argp], {
  baudRate: 115200,
  dataBits: 8,
  parity: 'none',
  flowControl: false,
  autoOpen: false
});
port.on('error', function(err) {
    console.log('> Port error: ', err.message);
});
port.on('data', function (data) {
  if(!quiet)
    process.stdout.write(data.toString().replace("\n", ''));
});

port.open(function(err) {
  if(err){
    console.log("> Error: ", err.message);
  } else {
    console.log('> Starting');
    setTimeout(scan, timeinit);
  }
});

function send(str){
  console.log('> Sending: ' + str);
  return new Promise((resolve, reject) => {
    port.write(str, function(err){
      if(err){
        console.log('> Send error: ', err.message);
        reject();
      } else {
        setTimeout(resolve, timeout);
      }
    });
  });
}

function leftPad(str, width, fill){
  if(!fill) fill = '0';
  while(str.length < fill)
    str = fill + str;
  return str;
}

let onTransferDone = null;
gphoto2.stdout.on('data', function(data){
  if(data.indexOf('Deleting') != -1){
    if(onTransferDone)
      onTransferDone();
  }
});
let onTransferError = null;
gphoto2.stderr.on('data', function(data){
  console.log('> GPhoto error: ' + data);
  if(onTransferError)
    onTransferError();
});
gphoto2.on('exit', function(code, signal){
  if(code){
    console.log('> GPhoto exited with code ' + code + ', signal ' + signal);
  } else {
    console.log('> Done');
    gphoto2.stdin.end();
  }
});
gphoto2.on('close', function(code){
  console.log('Closed with code ' + code);
  process.exit(0);
});

function trigger(num, led){
  let background = arguments.length == 0;
  return new Promise((resolve, reject) => {
    console.log('> Triggering camera');
    let fname = background ? 'ambiant.jpg' : num + '_' + led + '.jpg';

    // trigger download from shell
    onTransferDone = function(){
      // it worked => we can move the file
      fs.renameSync('capture.jpg', path.join(outputDir, fname));
      onTransferDone = null; //
      resolve();
    };
    onTransferError = function(){
      // something failed
      reject();
    };
    gphoto2.stdin.write('capture-image-and-download\n');
  });
}

async function scan(){
  // get background picture
  await trigger();

  // get individual pixel pictures
  for(let i = argp + 1, j = 0; i < process.argv.length; ++i, ++j){
    await send('p ' + process.argv[i]);

    if(noPictures)
      continue;
    // trigger camera
    await trigger(j, process.argv[i]);
  }

  // clear neopixels
  await send('c');

  // quit gphoto process
  gphoto2.stdin.write('quit\n');
}
