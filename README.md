# Stereo Capture with Arduino + Neopixels + GPhoto2

Requirements:

* gphoto2
* nodejs (v10) + npm
* serialport

## Installing GPhoto2 with brew

```
brew install gphoto2
```

## Installing serialport with npm

```
npm install serialport
```

## Running a slow scan with pixels 50, 60 and 100

```
node --use_strict scan.js -t 1000 -o cap1 -q /dev/cu.usbmodem1411 50 60 100
```

* `-t 1000` is the delay between sending a command to arduino and assuming reception
* `-o cap1` is the output directory (gets created if it does not exist)
* `-q` to silence all the arduino outputs
* `/dev/cu.usbmodem1411` is the path to the arduino serial port (change to your value!)
* `50 60 100` is the list of leds to trigger a picture for

## Running a fast scan with all pixels

```
node --use-strict fastscan.js -t 500 -o cap3 -q /dev/cu.usbmodem1411 {36..62} {80..103}
```

* `fastscan.js` uses a single gphoto2 session so the delay is minimal
* `-t 500` only waits for 500ms for the Arduino

This leads to 51 images in `cap3`, captured in around a minute.

